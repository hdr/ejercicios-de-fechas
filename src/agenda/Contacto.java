package agenda;

import cal.Fecha;

public class Contacto {

	private String nombre; // debería ser, pero no es: nombre = "";
	private Fecha fechaDeNacimiento;

	public Contacto(String nombre, Fecha fechaDeNacimiento) {
		this.nombre = nombre;
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public static void main(String[] args) {
		Contacto c = new Contacto("Juan Carlos", new Fecha(8, 3, 2001));

		System.out.println(c.nombre);
		System.out.println(c.fechaDeNacimiento);
	}

}
