package cal;

public class Main {
	
	public static void main(String[] args) {
		Fecha f1 = new Fecha(26, 6, 2011);
		Fecha f2 = new Fecha(27, 1, 1756);
//		Fecha f3 = new Fecha(57, -38, 1986);
	
		System.out.println("días en la fecha f2: " + f2.getDia());
		
		System.out.println(f1);
//		f2.imprimir();
//		System.out.println(f3);
		
		f1.avanzarDia();
		f1.avanzarDia();
		f1.avanzarDia();
		f1.avanzarDia();
		f1.avanzarDia();
		f1.avanzarDia();
		
		System.out.println("días en la fecha f1: " + f1.getDia());

//		f3.avanzarDia();
//		System.out.println("días en la fecha f3: " + f3.dia);
		
		// Cuál de las siguientes líneas está bien?
		System.out.println(Fecha.esBisiesto(2020));
//		System.out.println(f3.esBisiesto(2020));

		// Cuál de las siguientes líneas está bien?
//		int cantidadDeDias = f3.diasDelMes(2, 2020);
		int cantidadDeDias = Fecha.diasDelMes(2, 2020);

		// Cuál de las siguientes líneas está bien?
//		boolean okay = f2.esValida();
//		boolean okay = Fecha.esValida();
		
		// Cuál de las siguientes líneas está bien?
		// son válidas ambas, pero la línea 43 queda mejor
		Fecha.antesQue(f2, f1);
		f2.antesQue(f1);
	}

}
