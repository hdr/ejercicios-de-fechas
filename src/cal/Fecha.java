package cal;

public class Fecha {

	// invariante de representación:
	
	// anio >= 1
	// 1 <= mes <= 12
	// 1 <= dia <= 31, para mes == 1, 3, 5, 7, 8, 10, 12
	// 1 <= dia <= 30, para mes == 4, 6, 9, 11
	// 1 <= dia <= 29, para mes == 2, y anio es bisiesto
	// 1 <= dia <= 28, para mes == 2, y anio *no* es bisiesto
	
	private int dia;
	private int mes;
	private int anio;
	
	public Fecha() {
		this.dia = 1;
		this.mes = 1;
		this.anio = 1970;
	}

	public Fecha(int dia, int mes, int anio) {
		this.dia = dia;
		this.mes = mes;
		this.anio = anio;
		
		if (!this.esValida()) {
			throw new IllegalArgumentException("fecha inválida: " + this);
		}
	}

//	public void imprimir() {
//		System.out.println(dia + "/" + mes + "/" + anio);
//	}

	public String toString() {
		return dia + "/" + mes + "/" + anio;
	}

	public static boolean esBisiesto(int anio) {
		return anio % 4 == 0 && anio % 100 != 0 || anio % 400 == 0;
	}

	public static int diasDelMes(int mes, int anio) {
		if (mes < 1 || mes > 12) {
			throw new IllegalArgumentException("mes " + mes + " inválido");
		}
		if (anio < 1) {
			throw new IllegalArgumentException("año " + anio + " inválido");

		}
		
		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
			return 31;
		}
		if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
			return 30;
		}
		if (mes == 2 && esBisiesto(anio)) {
			return 29;
		} else {
			return 28;
		}
	}

	private boolean esValida() {
		if (this.mes < 1 || this.mes > 12) {
			return false;
		}
		if (this.dia < 1 || this.dia > diasDelMes(this.mes, this.anio)) {
			return false;
		}
		return true;
	}

//	public void avanzarDia() {
//		this.dia++;
//	}

	public void avanzarDia() {
		int ultimoDiaDelMes = diasDelMes(mes, anio);
		if (dia < ultimoDiaDelMes) {
			dia++;
		} else if (dia == ultimoDiaDelMes && mes < 12) {
			dia = 1;
			mes++;
		} else if (dia == ultimoDiaDelMes && mes == 12) {
			dia = 1;
			mes = 1;
			anio++;
		}
		
		if (!this.esValida()) {
			throw new IllegalArgumentException("fecha inválida: " + this);
		}
	}

	// TODO
	public boolean antesQue(Fecha otra) {
		return false;
	}

	// TODO
	public static boolean antesQue(Fecha f1, Fecha f2) {
		return false;
	}
	
	public int getDia() {
		return dia;
	}

	public int getMes() {
		return mes;
	}

	public int getAnio() {
		return anio;
	}

	// Esto se suele usar sólo para probar cosas,
	// una vez que ven que todo funca bien, bórrenlo.
	public static void main(String[] args) {
		Fecha f1 = new Fecha();
		Fecha f2 = new Fecha(26, 6, 2011);

		System.out.println(f1);
		System.out.println("te fuiste a la B el " + f2);
		
		System.out.println(f2);

//		f1.imprimir();
//		f2.imprimir();
	}

}
